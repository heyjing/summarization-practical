"""
Script to run existing summarization models from hugging face.
"""

from typing import Optional
from transformers import pipeline
from datasets import load_dataset
from transformers import AutoTokenizer, AutoModelForSeq2SeqLM


def get_data(
    dataset_name: str,
    split: str = "train",
    language: Optional[str] = None,
):
    # type of data: <class 'datasets.arrow_dataset.Dataset'>
    data = load_dataset(dataset_name, language)[split]
    return data


def get_summarizer(
    model_name: str, device: int, batch_size: int, max_length: Optional[int] = None
):
    # Based on user inputs, provide additional model max length for proper truncation
    if not max_length:
        tokenizer = AutoTokenizer.from_pretrained(model_name)
    else:
        tokenizer = AutoTokenizer.from_pretrained(
            model_name, model_max_length=max_length
        )
    model = AutoModelForSeq2SeqLM.from_pretrained(model_name)
    summarizer = pipeline(
        "summarization",
        model=model,
        tokenizer=tokenizer,
        device=device,
        batch_size=batch_size,
    )
    return summarizer


if __name__ == "__main__":

    # Klexikon
    source_text = get_data("dennlinger/klexikon")[:2].get("wiki_sentences")
    source_text = [" ".join(x) for x in source_text]

    # summ = get_summarizer("mrm8488/bert2bert_shared-german-finetuned-summarization")
    summ = get_summarizer(
        "ml6team/mt5-small-german-finetune-mlsum",
        device=-1,
        batch_size=1,
        max_length=512,
    )
    print(source_text)
    # Provide truncation here to avoid running out of memory
    print(
        summ(
            source_text, max_length=130, min_length=30, do_sample=False, truncation=True
        )
    )
