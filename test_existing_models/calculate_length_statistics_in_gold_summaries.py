"""
This is a script that calculates the mean, median length of the gold summaries and source texts in Klexikon, Mlsum and Wiki_lingua.
A plot that compares the length difference of the three datasets can be generated. Run it by click "Run Cell".
"""
# %%
from typing import Dict, List
from matplotlib import pyplot as plt
from itertools import chain
import statistics
from transformers import T5Tokenizer
from tqdm import tqdm
import spacy
import test_existing_models
import split_wikilingua

nlp = spacy.load("de_core_news_sm", disable=("ner",))
tokenizer = T5Tokenizer.from_pretrained("google/mt5-small")


def calculate_relevant_statistics(texts: List[str]):

    # regular tokenization
    summary_word_len: List[int] = [len(nlp(i)) for i in tqdm(texts)]
    # subword tokenization
    summary_token_len = [
        len(tokenizer(i, add_special_tokens=True)["input_ids"]) for i in tqdm(texts)
    ]

    print("Average word length: ", f"{(statistics.mean(summary_word_len)):.4f}")
    print("Median word length: ", statistics.median(summary_word_len))
    print("Min word length: ", min(summary_word_len))
    print("Max word length: ", max(summary_word_len))

    print("Average token length: ", f"{(statistics.mean(summary_token_len)):.4f}")
    print("Median token length: ", statistics.median(summary_token_len))
    print("Min token length: ", min(summary_token_len))
    print("Max token length: ", max(summary_token_len))

    larger_elements = [element for element in summary_token_len if element > 512]
    percentage_of_larger_elements = len(larger_elements) / len(summary_token_len)
    smaller_elements = [element for element in summary_token_len if element < 10]
    percentage_of_smaller_elements = len(smaller_elements) / len(summary_token_len)
    print(
        "Percentage of gold summaries with length more than 512: ",
        f"{percentage_of_larger_elements*100:.4f}%",
    )
    print(
        "Percentage of gold summaries with length smaller than 10: ",
        f"{percentage_of_smaller_elements*100:.4f}%",
    )


def draw_plot(texts: List[List[str]], labels: List[str]):
    if len(texts) != 0:
        for i in range(len(texts)):
            ys = [len(i.split()) for i in texts[i]][:300]
            xs = [x for x in range(len(ys))]
            plt.plot(xs, ys, label=labels[i])
        plt.legend()
        plt.show()


if __name__ == "__main__":
    # --------------------------wikilingua: train split has length 16082-----------------------
    # type of wiki_lingua_dataset_not_splited: <class 'datasets.arrow_dataset.Dataset'>

    train_set = split_wikilingua.get_splits_in_wikilingua("train")

    choice = input(
        "Press 1 to get the staticstics of the gold summaries or\npress 2 to get the staticstics of the source texts:\n"
    )

    if choice == "1":

        klexikon_gold_summaries: List[List[str]] = test_existing_models.get_data(
            "dennlinger/klexikon"
        )[:].get("klexikon_sentences")

        klexikon_gold_summaries: List[str] = [
            " ".join(x) for x in klexikon_gold_summaries
        ]
        print("--------------Klexikon gold summaries--------------------")
        calculate_relevant_statistics(klexikon_gold_summaries)

        mlsum_gold_summaries: List[str] = test_existing_models.get_data(
            "mlsum", language="de"
        )[:]["summary"]

        print("--------------Mlsum gold summaries--------------------")
        calculate_relevant_statistics(mlsum_gold_summaries)

        wiki_lingua_gold_summaries: List[str] = list(
            chain.from_iterable([i["summary"] for i in train_set[:]["article"]])
        )

        print("--------------Wikilingua gold summaries--------------------")
        calculate_relevant_statistics(wiki_lingua_gold_summaries)

        draw_plot(
            texts=[
                klexikon_gold_summaries,
                mlsum_gold_summaries,
                wiki_lingua_gold_summaries,
            ],
            labels=["klexikon", "mlsum", "wiki_lingua"],
        )

    if choice == "2":
        klexikon_source_texts: List[List[str]] = test_existing_models.get_data(
            "dennlinger/klexikon",
        )[:].get("wiki_sentences")

        klexikon_source_texts: List[str] = [" ".join(x) for x in klexikon_source_texts]

        print("--------------Klexikon source texts--------------------")
        calculate_relevant_statistics(klexikon_source_texts)

        mlsum_source_texts: List[str] = test_existing_models.get_data(
            "mlsum", language="de"
        )[:]["text"]

        print("--------------Mlsum source texts--------------------")
        calculate_relevant_statistics(mlsum_source_texts)

        wiki_lingua_source_texts: List[str] = list(
            chain.from_iterable([i["document"] for i in train_set[:]["article"]])
        )

        print("--------------Wikilingua source texts--------------------")
        calculate_relevant_statistics(wiki_lingua_source_texts)

        draw_plot(
            texts=[
                klexikon_source_texts,
                mlsum_source_texts,
                wiki_lingua_source_texts,
            ],
            labels=["klexikon", "mlsum", "wiki_lingua"],
        )


# %%
