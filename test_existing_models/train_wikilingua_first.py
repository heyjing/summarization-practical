"""
This is a script that trains the bert2bert model on the wikilingua dataset.
"""

from transformers import (
    AutoTokenizer,
    AutoModelForSeq2SeqLM,
    IntervalStrategy,
    Seq2SeqTrainer,
    Seq2SeqTrainingArguments,
    DataCollatorForSeq2Seq,
)

import split_wikilingua


def unnest_nested_list(example):
    example["article.summary"] = " ".join(example["article.summary"])
    example["article.document"] = " ".join(example["article.document"])
    return example


def preprocess_function(examples):
    inputs = [doc for doc in examples["article.document"]]

    model_inputs = tokenizer(
        inputs, max_length=max_input_length, truncation=True, padding=True
    )

    # Setup the tokenizer for targets
    with tokenizer.as_target_tokenizer():
        labels = tokenizer(
            examples["article.summary"],
            max_length=max_target_length,
            truncation=True,
            padding=True,
        )

    model_inputs["labels"] = labels["input_ids"]
    return model_inputs


if __name__ == "__main__":

    model = AutoModelForSeq2SeqLM.from_pretrained(
        "mrm8488/bert2bert_shared-german-finetuned-summarization"
    )
    tokenizer = AutoTokenizer.from_pretrained(
        "mrm8488/bert2bert_shared-german-finetuned-summarization", model_max_length=512
    )

    max_input_length = 512
    max_target_length = 128

    wikilingua_train_set = (
        split_wikilingua.get_splits_in_wikilingua("train")
        .flatten()
        .map(unnest_nested_list)
        .map(preprocess_function, batched=True)
    )
    wikilingua_valid_set = (
        split_wikilingua.get_splits_in_wikilingua("validation")
        .flatten()
        .map(unnest_nested_list)
        .map(preprocess_function, batched=True)
    )

    data_collator = DataCollatorForSeq2Seq(tokenizer=tokenizer, model=model)

    training_args = Seq2SeqTrainingArguments(
        output_dir="./trained_models/train_wikilingua",
        evaluation_strategy="epoch",
        learning_rate=5e-5,
        per_device_train_batch_size=8,
        per_device_eval_batch_size=8,
        weight_decay=0.0,
        save_total_limit=3,
        save_steps=500,
        num_train_epochs=5,
        predict_with_generate=True,
        fp16=True,
        logging_strategy=IntervalStrategy.STEPS,
        logging_nan_inf_filter=True,
    )

    trainer = Seq2SeqTrainer(
        model=model,  # the instantiated Transformers model to be trained
        args=training_args,  # training arguments, defined above
        train_dataset=wikilingua_train_set,  # training dataset
        eval_dataset=wikilingua_valid_set,  # evaluation dataset
        tokenizer=tokenizer,
        data_collator=data_collator,
    )

    trainer.train()
    trainer.save_model("./trained_models/train_wikilingua")
