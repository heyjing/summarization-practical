"""
This is a script that trains the bert2bert model on the klexikon dataset.
The prerequisite is that we have trained the bert2bert model on wikilingua.
"""

from transformers import (
    AutoTokenizer,
    AutoModelForSeq2SeqLM,
    IntervalStrategy,
    Seq2SeqTrainer,
    Seq2SeqTrainingArguments,
    DataCollatorForSeq2Seq,
)

import test_existing_models


def join_strings(example):
    example["wiki_sentences"] = " ".join(example["wiki_sentences"])
    example["klexikon_sentences"] = " ".join(example["klexikon_sentences"])
    return example


def preprocess_function(examples):
    inputs = [doc for doc in examples["wiki_sentences"]]

    model_inputs = tokenizer(
        inputs, max_length=max_input_length, truncation=True, padding=True
    )

    # Setup the tokenizer for targets
    with tokenizer.as_target_tokenizer():
        labels = tokenizer(
            examples["klexikon_sentences"],
            max_length=max_target_length,
            truncation=True,
            padding=True,
        )

    model_inputs["labels"] = labels["input_ids"]
    return model_inputs


if __name__ == "__main__":

    model = AutoModelForSeq2SeqLM.from_pretrained("./trained_models/train_wikilingua")
    tokenizer = AutoTokenizer.from_pretrained(
        "mrm8488/bert2bert_shared-german-finetuned-summarization", model_max_length=512
    )

    max_input_length = 512
    max_target_length = 128

    klexikon_train_dataset = (
        test_existing_models.get_data("dennlinger/klexikon", split="train")
        .map(join_strings)
        .map(preprocess_function, batched=True)
    )

    klexikon_validation_dataset = (
        test_existing_models.get_data("dennlinger/klexikon", split="validation")
        .map(join_strings)
        .map(preprocess_function, batched=True)
    )

    data_collator = DataCollatorForSeq2Seq(tokenizer=tokenizer, model=model)

    training_args = Seq2SeqTrainingArguments(
        output_dir="./trained_models/train_wikilingua_add_klexikon",
        evaluation_strategy="epoch",
        learning_rate=5e-5,
        per_device_train_batch_size=8,
        per_device_eval_batch_size=8,
        weight_decay=0.0,
        save_total_limit=3,
        save_steps=500,
        num_train_epochs=3,
        predict_with_generate=True,
        fp16=True,
        logging_strategy=IntervalStrategy.STEPS,
        logging_nan_inf_filter=True,
    )

    trainer = Seq2SeqTrainer(
        model=model,  # the instantiated Transformers model to be trained
        args=training_args,  # training arguments, defined above
        train_dataset=klexikon_train_dataset,  # training dataset
        eval_dataset=klexikon_validation_dataset,  # evaluation dataset
        tokenizer=tokenizer,
        data_collator=data_collator,
    )

    trainer.train()
    trainer.save_model("./trained_models/train_wikilingua_add_klexikon")
