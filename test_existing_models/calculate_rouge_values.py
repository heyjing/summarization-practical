from typing import List, Dict
from itertools import chain
import argparse
import sys
import torch
from nltk.stem.cistem import Cistem
from rouge_score import rouge_scorer, scoring
from tqdm import tqdm
import test_existing_models
import split_wikilingua


def get_suitable_summary_max_min_length_on_dataset(dataset_name: str):
    if dataset_name == "dennlinger/klexikon":
        max_length = 512
        min_length = 70
    elif dataset_name == "mlsum":
        max_length = 354
        min_length = 13
    elif dataset_name == "wiki_lingua":
        max_length = 512
        min_length = 50
    else:
        raise ValueError("Unsupported dataset name specified!")
    return max_length, min_length


def get_gold_summaries_from_dataset(dataset_name: str, split_name: str) -> List[str]:
    if split_name == "train" or split_name == "validation" or split_name == "test":
        if dataset_name == "dennlinger/klexikon":
            gold_summaries: List[List[str]] = test_existing_models.get_data(
                "dennlinger/klexikon", split=split_name
            )[:].get("klexikon_sentences")
            gold_summaries: List[str] = [" ".join(x) for x in gold_summaries]
        elif dataset_name == "mlsum":
            gold_summaries: List[str] = test_existing_models.get_data(
                "mlsum", language="de", split=split_name
            )[:]["summary"]
        elif dataset_name == "wiki_lingua":
            sub_dataset = split_wikilingua.get_splits_in_wikilingua(split_name)
            gold_summaries: List[str] = list(
                chain.from_iterable([i["summary"] for i in sub_dataset[:]["article"]])
            )
        else:
            raise ValueError("Unsupported dataset name specified!")
    else:
        raise ValueError("Unsupported split name specified!")
    return gold_summaries


def get_source_texts_from_dataset(dataset_name: str, split_name: str) -> List[str]:
    if split_name == "train" or split_name == "validation" or split_name == "test":
        if dataset_name == "dennlinger/klexikon":
            source_texts: List[List[str]] = test_existing_models.get_data(
                "dennlinger/klexikon", split=split_name
            )[:].get("wiki_sentences")
            source_texts: List[str] = [" ".join(x) for x in source_texts]
        elif dataset_name == "mlsum":
            source_texts: List[str] = test_existing_models.get_data(
                "mlsum", language="de", split=split_name
            )[:]["text"]
        elif dataset_name == "wiki_lingua":
            sub_dataset = split_wikilingua.get_splits_in_wikilingua(split_name)
            source_texts: List[str] = list(
                chain.from_iterable([i["document"] for i in sub_dataset[:]["article"]])
            )
        else:
            raise ValueError("Unsupported dataset name specified!")
    else:
        raise ValueError("Unsupported split name specified!")
    return source_texts


def get_scorer(fast: bool = False) -> rouge_scorer.RougeScorer:
    # Skip LCS computation for 10x speedup during debugging.
    if fast:
        scorer = rouge_scorer.RougeScorer(["rouge1", "rouge2"], use_stemmer=True)
    else:
        scorer = rouge_scorer.RougeScorer(
            ["rouge1", "rouge2", "rougeL"], use_stemmer=True
        )

    stemmer = Cistem(
        case_insensitive=True
    )  # Insensitive because RougeScorer lower cases anyways.
    scorer._stemmer = stemmer

    return scorer


def generate_summaries(
    summarizer, source_texts: List[str], max_length: int, min_length: int
) -> List[List[Dict]]:
    generated_summaries = []
    for source_text in tqdm(source_texts):
        generated_summaries.append(
            summarizer(
                source_text,
                max_length=max_length,
                min_length=min_length,
                do_sample=False,
                truncation=True,
            )
        )
    return generated_summaries


def generate_summaries_on_dataset(
    model_name: str, dataset_name: str, split_name: str
) -> List[str]:
    source_texts = get_source_texts_from_dataset(dataset_name, split_name)
    max_length, min_length = get_suitable_summary_max_min_length_on_dataset(
        dataset_name
    )
    device = 1 if torch.cuda.is_available() else -1

    summarizer = test_existing_models.get_summarizer(
        model_name=model_name,
        device=device,
        batch_size=8,
        max_length=512,
    )
    generated_summaries: List[List[Dict]] = generate_summaries(
        summarizer, source_texts, max_length=max_length, min_length=min_length
    )
    generated_summaries: List[str] = [
        summary[0]["summary_text"] for summary in generated_summaries
    ]

    return generated_summaries


def generate_average_rouge_scores(
    generated_summaries: List[str], gold_summaries: List[str]
) -> Dict[str, float]:

    aggregator = scoring.BootstrapAggregator()
    for (generated_summary, gold_summary) in zip(generated_summaries, gold_summaries):
        aggregator.add_scores(get_scorer().score(gold_summary, generated_summary))

    average_rouge_scores = aggregator.aggregate()
    return average_rouge_scores


def evaluate_model_on_datasets_test_split(model_name: str, dataset_name: str):
    generated_summaries: List[str] = generate_summaries_on_dataset(
        model_name, dataset_name, split_name="test"
    )
    gold_summaries: List[str] = get_gold_summaries_from_dataset(
        dataset_name, split_name="test"
    )
    scores = generate_average_rouge_scores(generated_summaries, gold_summaries)
    return scores


def parse_args():
    """
    Parse input arguments
    """
    parser = argparse.ArgumentParser(
        description="Evaluate rouge scores of different models on different datasets"
    )
    parser.add_argument(
        "model_name",
        choices=[
            "mrm8488/bert2bert_shared-german-finetuned-summarization",
            "ml6team/mt5-small-german-finetune-mlsum",
            "deutsche-telekom/mt5-small-sum-de-mit-v1",
        ],
        type=str,
    )
    parser.add_argument(
        "dataset_name",
        choices=["dennlinger/klexikon", "mlsum", "wiki_lingua"],
        type=str,
    )

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parse_args()
    model_name = args.model_name
    dataset_name = args.dataset_name

    scores = evaluate_model_on_datasets_test_split(model_name, dataset_name)
    print(scores)

    # If you want To generate and store summaries
    # generated_summaries = generate_summaries_on_dataset(
    #     model_name, dataset_name, "test"
    # )
