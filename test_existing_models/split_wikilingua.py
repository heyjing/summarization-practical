"""
This script is to split the wikilingua dataset into three subsets: training set, validation set and test set (8:1:1)
"""
from sklearn.model_selection import train_test_split
import test_existing_models


def get_dataset_splits(dataset):
    train_ValidAndTest = dataset.train_test_split(test_size=0.2, shuffle=False)
    valid_test = train_ValidAndTest["test"].train_test_split(
        test_size=0.5, shuffle=False
    )
    # return train, validation and test plits (8:1:1)
    return train_ValidAndTest["train"], valid_test["train"], valid_test["test"]


def get_splits_in_wikilingua(split: str):
    wiki_lingua_dataset_not_splited = test_existing_models.get_data(
        "wiki_lingua",
        language="german",
    )

    train_set, valid_set, test_set = get_dataset_splits(wiki_lingua_dataset_not_splited)

    if split == "train":
        return train_set
    elif split == "validation":
        return valid_set
    elif split == "test":
        return test_set
    else:
        raise ValueError("Unsupported split name specified!")


if __name__ == "__main__":

    train_set = get_splits_in_wikilingua("train")
    valid_set = get_splits_in_wikilingua("validation")
    test_set = get_splits_in_wikilingua("test")
    print(f"The length of training set is {len(train_set)}")
    print(f"The length of validation set is {len(valid_set)}")
    print(f"The length of testing set is {len(test_set)}")
