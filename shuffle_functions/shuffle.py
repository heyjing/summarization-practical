"""
Script to implement random shuffle and a shuffle that puts the actual "most similar sentences" in the beginning of the document.
"""
import numpy as np
from summaries.aligners import RougeNAligner
from typing import List


def fully_random_shuffle(lst: List[str], seed: int) -> List[str]:
    rng = np.random.default_rng(seed=seed)
    rng.shuffle(lst)
    return lst


def remove_duplicates_from_list(lst: List[str]) -> List[str]:
    seen = set()
    result = []
    for item in lst:
        if item not in seen:
            seen.add(item)
            result.append(item)
    return result


def similarity_shuffle(source_text: List[str], gold_summary: List[str]) -> List[str]:
    aligner = RougeNAligner(n=2, optimization_attribute="fmeasure")
    relevant_source_sentences = aligner.extract_source_sentences(
        summary=gold_summary, reference=source_text
    )
    relevant_source_sentences = [
        sentence.sentence for sentence in relevant_source_sentences
    ]
    relevant_source_sentences = remove_duplicates_from_list(relevant_source_sentences)
    shuffled_source_text = relevant_source_sentences[:]

    # Create lookup
    relevant_sentence_lookup = set(relevant_source_sentences)

    for sentence in source_text:
        if sentence not in relevant_sentence_lookup:
            shuffled_source_text.append(sentence)

    return shuffled_source_text
