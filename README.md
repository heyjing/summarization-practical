# _Domain Generalization of German Summarization Systems_

Beginner's practical, Sommersemester 2022  
Author: Jing Fan  
Supervisor: Dennis Aumiller

# 1 Introduction

## 1.1 Introduction to the topic

**Automatic text summarization** is an important task in the field of natural language processing. It is considered challenging, because

- the generated summaries need to be concise, clear, coherent, grammatically correct and factually consistent.
- the evaluation of text summarization is difficult. For example, semantically and syntactically incorrect sentences are ignored while scoring using traditional evaluation metrics, such as Rouge.

There are two types of text summarization, extractive and abstractive methods. In this practical, we only concentrate on <ins>abstractive summarization systems</ins>.

- **extractive models**: a set of important sentences are directly extracted from the input document.
- **abstractive models**: new sentences are generated from the information extracted from the input.

Compared to English, **German text summarization** is more challenging, because fewer summarization research is carried out for the German language and there are fewer suitable German datasets for this task.

## 1.2 Goals of this project

To answer questions around the task of summarizing German documents:

1. What kinds of problems do the current German summarization models have?
2. Can we fine-tune a German summarization model that works for different domains?
3. Does fine-tuning improve summarization performance?
4. Do we have any ideas and recommendations for future research?

## 1.3 Structure of this project

- **_test_existing_models_**:

  - _[calculate_length_statistics_in_gold_summaries.py](https://git-dbs.ifi.uni-heidelberg.de/practicals/2022-jing-fan/-/blob/main/test_existing_models/calculate_length_statistics_in_gold_summaries.py)_: This script calculates the mean, median max and min word/subword length of the gold summaries and source texts in klexikon, mlsum and wiki_lingua. A figure that compares the length difference of the three datasets can be generated.
  - _[calculate_rouge_values](https://git-dbs.ifi.uni-heidelberg.de/practicals/2022-jing-fan/-/blob/main/test_existing_models/calculate_rouge_values.py)_: This script evaluates a model's performance on the three existing datasets. It uses command line interface and users can simply pass a model's name (string) and a dataset's name (string) and then evaluate the model's performance on the test set of the input dataset through rouge values.
  - _[split_wikilingua.py](https://git-dbs.ifi.uni-heidelberg.de/practicals/2022-jing-fan/-/blob/main/test_existing_models/split_wikilingua.py)_: This script splits the wikilingua dataset into three subsets: training set, validation set and test set (8:1:1).
  - _[text_existing_models.py](https://git-dbs.ifi.uni-heidelberg.de/practicals/2022-jing-fan/-/blob/main/test_existing_models/test_existing_models.py)_: This script helps run existing summarization models from hugging face. The `get_data` function loads a dataset from hugging face and the `get_summarizer` function loads the summarizer of respective models.
  - _[train_wikilingua_first.py](https://git-dbs.ifi.uni-heidelberg.de/practicals/2022-jing-fan/-/blob/main/test_existing_models/train_wikilingua_first.py)_: This script trains the `mrm8488/bert2bert_shared-german-finetuned-summarization` model on the wikilingua dataset.
  - _[train_klexikon_based_on_wikilingua.py](https://git-dbs.ifi.uni-heidelberg.de/practicals/2022-jing-fan/-/blob/main/test_existing_models/train_klexikon_based_on_wikilingua.py)_: After the `mrm8488/bert2bert_shared-german-finetuned-summarization` model is trained on the wikilingua dataset, we use this script to continue training on klexikon.
  - _[train_on_random_mlsum.py](https://git-dbs.ifi.uni-heidelberg.de/practicals/2022-jing-fan/-/blob/main/test_existing_models/train_on_random_mlsum.py)_: This script trains the `mrm8488/bert2bert_shared-german-finetuned-summarization` model on the fully random mlsum dataset.

- **_shuffle_functions_**:
  - _[shuffle.py](https://git-dbs.ifi.uni-heidelberg.de/practicals/2022-jing-fan/-/blob/main/shuffle_functions/shuffle.py)_: The `fully_random_shuffle` gets a list of strings and returns a randomized list of strings. The `similarity_shuffle` gets two list of strings (of source text and gold summary), it takes each sentence from the gold summary and finds the most similar sentence in the original source document, it returns a shuffled list of source text in which the actual "most similar sentences" are in the beginning of the document.
- **_tests_**:
  - _[test_shuffle.py](https://git-dbs.ifi.uni-heidelberg.de/practicals/2022-jing-fan/-/blob/main/tests/test_shuffle.py)_: There are two unittests that check the functionality of the two random functions in the directory **_shuffle_functions_**.

# 2 German summarization datasets

We have used three German summarization datasets in this practical, mlsum, wiki_lingua and klexikon. You can get a summary of these datasets from [Wiki page: Relevant Summarization Datasets for German](https://git-dbs.ifi.uni-heidelberg.de/practicals/2022-jing-fan/-/wikis/2-Relevant-Summarization-Datasets-for-German). These three datasets

- are all available from Hugging Face
- cover different domains
- differ greatly in length of source texts and gold summaries (see [Wiki page: Important Statistics about gold summaries and source texts in each dataset](<https://git-dbs.ifi.uni-heidelberg.de/practicals/2022-jing-fan/-/wikis/3-Important-Statistics-about-gold-summaries-and-source-texts-in-each-dataset-(updated-on-June-8th-2022)>))

# 3 German summarization models

In this practical, we have analyzed several abstractive models that are currently available in Hugging Face. They are trained on different datasets and have different architectures. The performance of these models are evaluated using rouge scores. A detailed comparison of these models can be found in the [Wiki page: Relevant Models for Evaluation](https://git-dbs.ifi.uni-heidelberg.de/practicals/2022-jing-fan/-/wikis/1-Relevant-Models-for-Evaluation).

# 4 Current problems of German summarization models

The current problems of the German summarization models were found through testing on different datasets and manual evaluation. It is worth notice that there is no work until now that analyzes these problems in detail. What’s more, most papers only evaluate on the basis of word pairs/n-grams and completely ignore these problems.

## 4.1 Models can only accurately process texts in specific domains

We have found that a model only get high scores when the domain of the input is the same as the domain of the training data. When the input is changed to other domain, the models' performance dropped significantly. You can find the performance of these models on different datasets in the [Wiki page: Rouge scores for existing datasets (test split) using different models](<https://git-dbs.ifi.uni-heidelberg.de/practicals/2022-jing-fan/-/wikis/5-Rouge-scores-for-existing-datasets-(test-split)-using-different-models-(updated-on-June-8th-2022)>).

## 4.2 Copying problems

Many summaries are just copies of the leading sentences of the source texts, probably because there is positional bias in the dataset used for training. Examples for copying problems can be found in

- [# 17 Track generation parameters and results](https://git-dbs.ifi.uni-heidelberg.de/practicals/2022-jing-fan/-/issues/17) and
- [# 23 Upload the word document with insights on samples](https://git-dbs.ifi.uni-heidelberg.de/practicals/2022-jing-fan/-/issues/23)

## 4.3 Content and grammatical errors

As mentioned earlier, most models and papers only evaluate on the basis of word pairs or [n grams](https://git-dbs.ifi.uni-heidelberg.de/practicals/2022-jing-fan/-/issues/2) and ignore completely the semantic and grammar of the generated summaries. This results in a lot of content and grammatical issues with the generated summaries. Some summaries are grammatically correct, but are not good summaries. Some summaries are factually incorrect or contain obvious grammatical errors. Examples for content and grammatical errors can be found in

- [# 17 Track generation parameters and results](https://git-dbs.ifi.uni-heidelberg.de/practicals/2022-jing-fan/-/issues/17) and
- [# 23 Upload the word document with insights on samples](https://git-dbs.ifi.uni-heidelberg.de/practicals/2022-jing-fan/-/issues/23)

## 4.4 Sentence/word repetition problems

There are also word repetition problems of automatic summaries. Examples for repetition problems can be found in [# 23 Upload the word document with insights on samples](https://git-dbs.ifi.uni-heidelberg.de/practicals/2022-jing-fan/-/issues/23).

All these problems make the quality of the automatic summaries much lower than human-generated summaries. For some of these issues, for example copying problems and repetition problems, we have contacted the authors of the relevant models, but they either didn't respond for over a month, or they have no idea how to avoid them.

# 5 Fine-tuned summarization model and evaluation results

We have fine-tuned two summarization models.

Insights from supervisor to these two models see [# 42 Model cards of the two new fine tuned models](https://git-dbs.ifi.uni-heidelberg.de/practicals/2022-jing-fan/-/issues/42)

## 5.1 Fine tuned bert2bert model on random shuffled mlsum dataset

**Goal**: try to move the summarization systems away from positional bias  
**Base model**: mrm8488/bert2bert_shared-german-finetuned-summarization  
**How was it trained**: The base model was trained on the whole shuffled mlsum training set (fully random shuffle)

Details see [Wiki page: Fine tuned bert2bert model on random shuffled mlsum dataset](https://git-dbs.ifi.uni-heidelberg.de/practicals/2022-jing-fan/-/wikis/7-Model-Card:-Fine-tuned-bert2bert-model-on-random-shuffled-mlsum-dataset).

## 5.2 Fine tuned bert2bert model on three datasets

**Goal**: achieve domain generalization of German summarization systems  
**Base model**: mrm8488/bert2bert_shared-german-finetuned-summarization  
**How was it trained**: The base model was trained on Mlsum dataset. We first trained the base model on wiki_lingua, and then continued training on klexikon.

Details see [Wiki page: Fine tuned bert2bert model on three datasets](https://git-dbs.ifi.uni-heidelberg.de/practicals/2022-jing-fan/-/wikis/8-Model-Card:-Fine-tuned-bert2bert-model-on-three-datasets).

## 5.3 General insights from the fine-tuning

- Fully random shuffle does not help solve the copying problems. The fine-tuned model copies sentences from various other places in the articles.
- Training on in-domain data drastically improves (in-domain) performance.
- During sequential fine-tuning, the final training dataset has the biggest influence on the language styles of the output, regardless of the input.
- As expected, the fine-tuned model performs significantly worse on the mlsum test split, which indicates that the baseline scores are unreasonably high due to a data bias.

# 6 Future directions

- Mix domain-specific datasets into one aggregated dataset and train on a random shuffle to avoid overfitting.
- Use prompts to steer generation (e.g., "Summarize Wikipedia: ")
- Use different base models for fine-tuning (e.g., monolingual German-t5)
- Use similarity shuffle to avoid positional bias

# 7 What I have learned in this practical

1. Usage and training of neural summarization models
2. Much experience in software engineering

# 8 Useful resources

- [How to open a merge request](https://git-dbs.ifi.uni-heidelberg.de/practicals/2022-jing-fan/-/issues/6)
- [How to access to our GPU server eirene](https://git-dbs.ifi.uni-heidelberg.de/practicals/2022-jing-fan/-/issues/8)
- [How to run unittests](https://git-dbs.ifi.uni-heidelberg.de/practicals/2022-jing-fan/-/issues/13)
- [How to use tmux to open many sessions](https://git-dbs.ifi.uni-heidelberg.de/practicals/2022-jing-fan/-/issues/22)
