import unittest

import shuffle


class TestShuffleMethods(unittest.TestCase):
    def test_fully_random_shuffle(self):
        example_input = ["1", "2", "3", "4", "5"]
        result = shuffle.fully_random_shuffle(example_input, 42)
        expected_result = ["5", "3", "4", "2", "1"]
        self.assertEqual(expected_result, result)

    def test_similarity_shuffle(self):
        example_input = [
            "ABBA ist eine schwedische Popgruppe, die aus den damaligen Paaren Agnetha Fältskog und Björn Ulvaeus sowie Benny Andersson und Anni-Frid Lyngstad besteht und sich 1972 in Stockholm formierte.",
            "Sie gehört mit rund 400 Millionen verkauften Tonträgern zu den erfolgreichsten Bands der Musikgeschichte.",
            "Bis in die 1970er Jahre hatte es keine andere Band aus Schweden oder Skandinavien gegeben, der vergleichbare Erfolge gelungen waren.",
            "Sie hat die Geschichte der Popmusik mitgeprägt",
            'Dieses einfache, sehr markante und prägnante Wort "ABBA" ist ein Palindrom und zugleich ein Akronym, das sich aus den Anfangsbuchstaben der vier Vornamen der Bandmitglieder in folgender Reihenfolge ableitet:',
            "Agnetha, Björn, Benny, Anni-Frid = ABBA.",
        ]

        gold_summary = [
            "ABBA war eine Musikgruppe aus Schweden.",
            "Ihre Musikrichtung war die Popmusik.",
            "Der Name entstand aus den Anfangsbuchstaben der Vornamen der Mitglieder, Agnetha, Björn, Benny und Anni-Frid.",
        ]

        expected_result = [
            "ABBA ist eine schwedische Popgruppe, die aus den damaligen Paaren Agnetha Fältskog und Björn Ulvaeus sowie Benny Andersson und Anni-Frid Lyngstad besteht und sich 1972 in Stockholm formierte.",
            "Sie hat die Geschichte der Popmusik mitgeprägt",
            "Agnetha, Björn, Benny, Anni-Frid = ABBA.",
            "Sie gehört mit rund 400 Millionen verkauften Tonträgern zu den erfolgreichsten Bands der Musikgeschichte.",
            "Bis in die 1970er Jahre hatte es keine andere Band aus Schweden oder Skandinavien gegeben, der vergleichbare Erfolge gelungen waren.",
            'Dieses einfache, sehr markante und prägnante Wort "ABBA" ist ein Palindrom und zugleich ein Akronym, das sich aus den Anfangsbuchstaben der vier Vornamen der Bandmitglieder in folgender Reihenfolge ableitet:',
        ]

        result = shuffle.similarity_shuffle(example_input, gold_summary)

        self.assertEqual(expected_result, result)
